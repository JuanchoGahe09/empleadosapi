package com.empleadosApi.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empleadosApi.entity.Empleado;

public interface EmpleadoRepo extends JpaRepository<Empleado, Integer>{

}
