package com.empleadosApi.mapper;

import com.empleadosApi.dto.EmpleadoDTO;
import com.empleadosApi.entity.Empleado;

public class EmpleadoMapper {
	
    public static EmpleadoDTO toDto(Empleado empleado) {
        EmpleadoDTO dto = new EmpleadoDTO();
        dto.setCodigo(empleado.getCodigo());
        dto.setApellidos(empleado.getApellidos());
        dto.setCedula(empleado.getCedula());
        dto.setEdad(empleado.getEdad());
        dto.setFecha_nacimiento(empleado.getFecha_nacimiento());
        dto.setNombres(empleado.getNombres());
        dto.setTelefono(empleado.getTelefono());
        
        return dto;
    }

    public static Empleado toEntity(EmpleadoDTO dto) {
    	Empleado empleado = new Empleado();
    	empleado.setCodigo(dto.getCodigo());
    	empleado.setApellidos(dto.getApellidos());
    	empleado.setCedula(dto.getCedula());
    	empleado.setEdad(dto.getEdad());
    	empleado.setFecha_nacimiento(dto.getFecha_nacimiento());
    	empleado.setNombres(dto.getNombres());
    	empleado.setTelefono(dto.getTelefono());
    	
        return empleado;
    }


}
