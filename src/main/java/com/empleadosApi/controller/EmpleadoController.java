package com.empleadosApi.controller;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleadosApi.dto.EmpleadoDTO;
import com.empleadosApi.entity.Empleado;
import com.empleadosApi.mapper.EmpleadoMapper;
import com.empleadosApi.service.EmpleadoService;



@RestController
@RequestMapping("empleados")
public class EmpleadoController {
	
	@Autowired
	private EmpleadoService empleadoservice;
	
    @GetMapping("/listar/")
    public List<EmpleadoDTO> listar() {
        List<Empleado> empleados = empleadoservice.listar();
        try {
        return empleados.stream().map(EmpleadoMapper::toDto).collect(Collectors.toList());
        } catch (Exception ex) {
        	ex.printStackTrace();
        	return Collections.emptyList();
        }
    }
	
	@PostMapping("/insertar/")
	public Empleado insertar(@RequestBody Empleado emp) {
		try {
			return empleadoservice.insertar(emp);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	@PutMapping("/actualizar/")
	public Empleado actualizar(@RequestBody Empleado emp) {
		try {
			return empleadoservice.actualizar(emp);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	@DeleteMapping("/eliminar/")
	public void eliminar (@RequestBody Empleado emp) {
		empleadoservice.eliminar(emp);
	}

}
